# Android iPerf Agent

This application acts as a wrapper for the iPerf measuring tool. This application allows the usage of iPerf on Android terminals remotelly, thus enabling the usage of Android terminals in automated performance test. Additionally, the application can be used directly from the user interface.

## Usage

The application can be used on the terminal with the provided user interface, or remotely through adb commands.

### Adb commands

The application can be used remotely sending intents to the service via the startservice command of adb:

`adb shell am startservice -n com.uma.iperf/.iPerfService`

The application accepts the following intents (-a):

`com.uma.iperf.CLIENTSTART (Requires parameters)`

`com.uma.iperf.SERVERSTART (Requires parameters)`

`com.uma.iperf.CLIENTSTOP`

`com.uma.iperf.SERVERSTOP`

Parameters sent to the iPerf binary are passed as a comma separated string (with no spaces) using the intent extra (-e) `com.uma.iperf.PARAMETERS`.

In some cases, it is necessary to specify the user of the command with --user 0, this flag does not seem to have any adverse effect, so it is recommended to use it always.

#### Examples
`adb shell am startservice -n com.uma.iperf/.iPerfService -a com.uma.iperf.CLIENTSTOP`

`adb shell am startservice -n com.uma.iperf/.iPerfService -a com.uma.iperf.CLIENTSTART -e com.uma.iperf.PARAMETERS "-c,127.0.0.1,-t,35,-w,4000K,-p,5002,-l,1470,-f,m,-P,1,-i,1" --user 0`

### Device Interface

The application can be used through the device interface. The application is divided into two parts, one for using iPerf as a server mode and the other one for using it as a client. Both parts are configured pressing the Parameters button and executed pressing the Start button. 

Once any of the iPerf modes are initiated, its corresponding Log will be updated during the execution. 

## Authors

* Alberto Salmeron Moreno
* Bruno Garcia Garcia
* Gonzalo Chica Morales

## License

Copyright 2020 MORSE Research Group - University of Malaga

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This application contains iPerf2, a tool for measuring Internet bandwidth performance, in binary form. iPerf2 is licensed under the revised BSD license. Full contents of the iPerf2 license can be seen below and in the 'License' option of the application menu.

### iPerf2 license

Copyright (c) 1999,2000,2001,2002,2003,2004,2005
The Board of Trustees of the University of Illinois
All Rights Reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software (Iperf) and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 - Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.

 - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimers in the documentation and/or other materials provided with the distribution.

 - Neither the names of the University of Illinois, NCSA, nor the names of its contributors may be used to endorse or promote products derived from this Software without specific prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE CONTIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n

National Laboratory for Applied Network Research
National Center for Supercomputing Applications
University of Illinois at Urbana-Champaign
http://www.ncsa.uiuc.edu

