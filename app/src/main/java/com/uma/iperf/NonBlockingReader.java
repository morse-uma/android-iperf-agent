/**
 * Developed by the University of Málaga
 *
 * Alberto Salmerón Moreno
 * Bruno García García
 * Gonzalo Chica Morales
 */

package com.uma.iperf;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NonBlockingReader {
    private AutoReader task;

    public NonBlockingReader(InputStreamReader input) {
        task = new AutoReader(input);
        // Use executeOnExecutor, otherwise the task will be executed *after*
        // the caller has finished running, which does not work very well...
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public boolean finished() {
        return task.finished();
    }

    public boolean ready() {
        return task.ready();
    }

    public String read() {
        return task.read();
    }

    public void close() {
        task.cancel(true);
    }

    private class AutoReader extends AsyncTask<Void, Void, Integer> {
        private final Object lock = new Object();
        private BufferedReader input;
        private StringBuffer output;
        private boolean eof = false;
        private InputStreamReader reader;

        public AutoReader(InputStreamReader r) {
            reader = r;
            output = new StringBuffer();
        }

        protected Integer doInBackground(Void... voids) {
            int read;
            char[] buffer = new char[4096];

            synchronized (lock) {
                eof = false;
            }

            try {
                input = new BufferedReader(reader);
                while ((read = input.read(buffer)) > 0 && !isCancelled()) {
                    synchronized (lock) {
                        output.append(buffer, 0, read);
                    }
                }
                synchronized (lock) {
                    eof = true;
                    input.close();
                }
            } catch (IOException e) {
                Log.e("NonBlockingReader", e.getMessage());
                synchronized (lock) {
                    eof = true;
                    output.delete(0, output.length());
                }
            }
            return 0;
        }

        public boolean finished() {
            synchronized (lock) {
                return eof;
            }
        }

        public boolean ready() {
            synchronized (lock) {
                return output.length() > 0;
            }
        }

        public String read() {
            String res;
            synchronized (lock) {
                res = output.toString();
                output.delete(0, output.length());
            }
            return res;
        }
    }
}
