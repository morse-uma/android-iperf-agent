/**
 * Developed by the University of Málaga
 *
 * Alberto Salmerón Moreno
 * Bruno García García
 * Gonzalo Chica Morales
 */

package com.uma.iperf;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class NoticeDialog extends DialogFragment {
    static NoticeDialog newInstance(boolean isLicense) {
        NoticeDialog notice = new NoticeDialog();
        Bundle args = new Bundle();
        args.putBoolean("isLicense", isLicense);
        notice.setArguments(args);
        return notice;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        boolean isLicense = bundle.getBoolean("isLicense");

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.notice, null);
        TextView textView = (TextView) view.findViewById(R.id.noticeText);
        if (isLicense) {
            ImageView image = (ImageView) view.findViewById(R.id.morseImage);
            ImageView image2 = (ImageView) view.findViewById(R.id.umaImage);
            image.setVisibility(View.GONE);
            image2.setVisibility(View.GONE);
            TextView copyright = (TextView) view.findViewById(R.id.copyright);
            copyright.setVisibility(View.GONE);
            textView.setText(R.string.license);
        } else {
            textView.setText(R.string.about);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view)
                .setCancelable(true)
                .setNegativeButton("Return", new CancelListener());
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    private static class CancelListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
        }
    }
}
