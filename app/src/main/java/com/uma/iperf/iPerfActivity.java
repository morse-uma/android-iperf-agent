/**
 * Developed by the University of Málaga
 *
 * Alberto Salmerón Moreno
 * Bruno García García
 * Gonzalo Chica Morales
 */

package com.uma.iperf;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class iPerfActivity extends AppCompatActivity {
    private static String LOG = "iperf." + iPerfActivity.class.getSimpleName();
    private static String MESSAGE = "iperf.Message";
    private static String APP_FOLDER;
    private static InterfaceIds serverIds;
    private static InterfaceIds clientIds;
    private LogReceiver logReceiver;
    private StatusReceiver statusReceiver;

    // Utility methods for sending broadcast messages to the application.
    public static void broadcast(LocalBroadcastManager manager, int mode, String message) {
        Intent logIntent = new Intent(iPerfService.LOG_BROADCAST_ACTION);
        logIntent.putExtra(iPerfService.LOG_IPERF_MODE, mode);
        logIntent.putExtra(iPerfService.LOG_STRING, message + "\n");

        manager.sendBroadcast(logIntent);
    }

    public static void notifyStatusChange(LocalBroadcastManager manager) {
        manager.sendBroadcast(new Intent(iPerfService.UPDATE_BROADCAST_ACTION));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iperf);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorBlack));
        toolbar.setTitle(R.string.app_name);
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        serverIds = new InterfaceIds();
        serverIds.Log = (TextView) findViewById(R.id.serverTextView);
        serverIds.Scroll = (ScrollView) findViewById(R.id.serverScroll);
        serverIds.StartStopButton = (Button) findViewById(R.id.serverButton);
        serverIds.SetParamsButton = (Button) findViewById(R.id.serverParamButton);
        serverIds.ParametersField = (TextView) findViewById(R.id.serverParameters);

        clientIds = new InterfaceIds();
        clientIds.Log = (TextView) findViewById(R.id.clientTextView);
        clientIds.Scroll = (ScrollView) findViewById(R.id.clientScroll);
        clientIds.StartStopButton = (Button) findViewById(R.id.clientButton);
        clientIds.SetParamsButton = (Button) findViewById(R.id.clientParamButton);
        clientIds.ParametersField = (TextView) findViewById(R.id.clientParameters);

        APP_FOLDER = getFilesDir().getAbsolutePath();

        if (savedInstanceState != null) { // Restore state
            serverIds.ParametersField.setText(savedInstanceState.getString("ServerParams"));
            clientIds.ParametersField.setText(savedInstanceState.getString("ClientParams"));
            serverIds.Log.setText(savedInstanceState.getString("ServerLog"));
            clientIds.Log.setText(savedInstanceState.getString("ClientLog"));
        } else { // Initialize
            Log.d(LOG, "Checking permissions");

            boolean granted = checkPermissions();

            if (granted) {
                Log.d(LOG, "Permission granted");
            } else {
                serverIds.Log.append("No WRITE_EXTERNAL_STORAGE permission, cannot create log files.\n");
            }

            boolean exists = new File(APP_FOLDER + "/iperf").exists();

            if (!exists) {
                serverIds.Log.append("Copying iPerf binary to application folder...\n");
                copyIperf();
                serverIds.Log.append("Done. Setting permissions...\n");
                setIPerfPermissions();
            } else {
                serverIds.Log.append("iPerf binary found.\n");
            }
            updateStatus();

            serverIds.Log.append("Initialization completed.\n\n");
        }
    }

    private boolean checkPermissions() {
        Context context = getApplicationContext();
        String[] permissions = new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE };
        boolean passed = true;

        for (String permission : permissions) {
            passed = passed && checkOnePermission(context, permission);
        }

        if (!passed) {
            ActivityCompat.requestPermissions(this, permissions, 100);
        }

        return passed;
    }

    private boolean checkOnePermission(Context context, String permission) {
        boolean granted = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        Log.d(LOG, permission + " granted: " + granted);
        return granted;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_button:
                Log.d(LOG, "About");
                NoticeDialog about = NoticeDialog.newInstance(false);
                about.show(getSupportFragmentManager(), "AboutNotice");
                return true;
            case R.id.license_button:
                Log.d(LOG, "License");
                NoticeDialog license = NoticeDialog.newInstance(true);
                license.show(getSupportFragmentManager(), "LicenseNotice");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("ServerParams", serverIds.ParametersField.getText().toString());
        savedInstanceState.putString("ClientParams", clientIds.ParametersField.getText().toString());
        savedInstanceState.putString("ServerLog", serverIds.Log.getText().toString());
        savedInstanceState.putString("ClientLog", clientIds.Log.getText().toString());
    }

    private void copyIperf() {
        InputStream in;
        try {
            String arch = System.getProperty("os.arch").toLowerCase();

            serverIds.Log.append("System architecture is " + arch + ". ");

            String binary = "";
            if (arch.contains("arm") || arch.contains("arch64")) {
                binary = "iperf-arm";
            } else if (arch.contains("i686") || arch.contains("x86_64")) {
                binary = "iperf-x86";
            } else if (arch.contains("mips")) {
                binary = "iperf-mips";
            }

            if (binary != "") {
                serverIds.Log.append("Using " + binary + "\n");
                in = getResources().getAssets().open(binary);
            } else {
                throw new IOException(String.format("No valid binary for current architecture (%1s) has been found. " +
                        "Binaries for ARMv7, x86 and MIPS architectures are included.", arch));
            }
        } catch (IOException e2) {
            String message = "Error occurred while accessing application assets: " + e2.getMessage();
            serverIds.Log.append("\n" + message + "\n");
            Log.i(MESSAGE, "<<<Error:" + message + ">>>");
            return;
        }

        try {
            String outputPath = APP_FOLDER + "/iperf";
            OutputStream out = new FileOutputStream(outputPath, false);

            serverIds.Log.append("Writing " + outputPath + "\n");

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (IOException e) {
            String message = "IO Error while writting iPerf binary: " + e;
            Log.i(MESSAGE, "<<<Error:" + message + ">>>");
            serverIds.Log.append("\n" + message);
        }
    }

    private void setIPerfPermissions() {
        try {
            String outputPath = APP_FOLDER + "/iperf";
            Process processChmod = Runtime.getRuntime().exec("/system/bin/chmod 755 " + outputPath);
            processChmod.waitFor();
        } catch (IOException e) {
            String message = "IO Error while setting permissions: " + e;
            Log.i(MESSAGE, "<<<Error:" + message + ">>>");
            serverIds.Log.append("\n" + message);
        } catch (java.lang.InterruptedException e) {
            String message = "chmod process interrupted: " + e;
            Log.i(MESSAGE, "<<<Error:" + message + ">>>");
            serverIds.Log.append("\n" + message);
        }
    }

    public void serverParamsButtonClicked(View view) {
        Log.i(LOG, "Server Params Button Clicked");
        ParamsDialog dialog = ParamsDialog.newInstance(iPerfTask.SERVER);
        dialog.show(getSupportFragmentManager(), "ServerParamsDialog");
    }

    public void clientParamsButtonClicked(View view) {
        Log.i(LOG, "Client Params Button Clicked");
        ParamsDialog dialog = ParamsDialog.newInstance(iPerfTask.CLIENT);
        dialog.show(getSupportFragmentManager(), "ClientParamsDialog");
    }

    public void clientButtonClicked(View view) {
        Log.i(LOG, "Client Button Clicked");
        witchStatus(iPerfTask.CLIENT);
    }

    public void serverButtonClicked(View view) {
        Log.i(LOG, "Server Button Clicked");
        witchStatus(iPerfTask.SERVER);
    }

    private void witchStatus(int cs) {
        boolean isClient = (cs == iPerfTask.CLIENT);
        boolean active = (isClient) ? iPerfService.clientActive() : iPerfService.serverActive();
        Intent intent = new Intent(this, iPerfService.class);

        if (active) {
            intent.setAction(
                    isClient ? iPerfService.STOP_CLIENT_ACTION : iPerfService.STOP_SERVER_ACTION);
        } else {
            intent.setAction(
                    isClient ? iPerfService.START_CLIENT_ACTION : iPerfService.START_SERVER_ACTION);

            TextView paramField = isClient ? clientIds.ParametersField : serverIds.ParametersField;
            String parameters = paramField.getText().toString().replace(" ", ",");

            intent.putExtra(iPerfService.PARAMETERS_EXTRA, parameters);
        }

        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        statusReceiver = new StatusReceiver();
        IntentFilter filter = new IntentFilter(iPerfService.UPDATE_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(statusReceiver, filter);

        logReceiver = new LogReceiver();
        filter = new IntentFilter(iPerfService.LOG_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(logReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(statusReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(logReceiver);
    }

    private void updateStatus() {
        if (iPerfService.clientActive()) {
            clientIds.StartStopButton.setText("Stop");
        } else {
            clientIds.StartStopButton.setText("Start");
        }
        clientIds.ParametersField.setText(iPerfService.clientParameters());
        if (iPerfService.serverActive()) {
            serverIds.StartStopButton.setText("Stop");
        } else {
            serverIds.StartStopButton.setText("Start");
        }
        serverIds.ParametersField.setText(iPerfService.serverParameters());
    }

    public static class ParamsDialog extends DialogFragment {
        // For communication between Listeners and the dialog
        static boolean isClient;
        static EditText textField;

        static ParamsDialog newInstance(int cs) {
            ParamsDialog dialog = new ParamsDialog();
            Bundle args = new Bundle();
            args.putInt("cs", cs);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            Bundle bundle = getArguments();
            int cs = bundle.getInt("cs");

            ParamsDialog.isClient = (cs == iPerfTask.CLIENT);

            // Create and edit the contents of the view before adding it to the dialog
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.set_parameters, null);
            EditText field = (EditText) view.findViewById(R.id.parametersField);
            field.setText(isClient ? clientIds.ParametersField.getText() :
                    serverIds.ParametersField.getText());

            ParamsDialog.textField = field;

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(isClient ? "Client Parameters" : "Server Parameters")
                    .setView(view)
                    .setCancelable(true)
                    .setPositiveButton("Save", new SaveListener())
                    .setNegativeButton("Cancel", new CancelListener());
            Dialog dialog = builder.create();
            dialog.setCanceledOnTouchOutside(true);
            return dialog;
        }

        private static class SaveListener implements DialogInterface.OnClickListener {
            public void onClick(DialogInterface dialog, int id) {
                Log.i(LOG, "Save");
                if (ParamsDialog.isClient) {
                    clientIds.ParametersField.setText(ParamsDialog.textField.getText());
                } else {
                    serverIds.ParametersField.setText(ParamsDialog.textField.getText());
                }
                dialog.dismiss();
            }
        }

        private static class CancelListener implements DialogInterface.OnClickListener {
            public void onClick(DialogInterface dialog, int id) {
                Log.i(LOG, "Cancel");
                dialog.dismiss();
            }
        }
    }

    private class InterfaceIds {
        public TextView Log;
        public ScrollView Scroll;
        public Button StartStopButton;
        public Button SetParamsButton;
        public TextView ParametersField;
    }

    private class StatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateStatus();
        }
    }

    private class LogReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int mode = intent.getIntExtra(iPerfService.LOG_IPERF_MODE, -1);
            String message = intent.getStringExtra(iPerfService.LOG_STRING);
            if (message != null && mode != -1) {
                final InterfaceIds cs;

                if (mode == iPerfTask.CLIENT) {
                    cs = clientIds;
                } else {
                    cs = serverIds;
                }

                cs.Log.append(message);
                cs.Scroll.post(new Runnable() {
                    public void run() {
                        cs.Scroll.smoothScrollTo(0, cs.Log.getBottom());
                    }
                });
            }
        }
    }
}
