/**
 * Developed by the University of Málaga
 *
 * Alberto Salmerón Moreno
 * Bruno García García
 * Gonzalo Chica Morales
 */

package com.uma.iperf;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

public class iPerfService extends Service {
    public final static String START_CLIENT_ACTION = "com.uma.iperf.CLIENTSTART";
    public final static String START_SERVER_ACTION = "com.uma.iperf.SERVERSTART";
    public final static String STOP_CLIENT_ACTION = "com.uma.iperf.CLIENTSTOP";
    public final static String STOP_SERVER_ACTION = "com.uma.iperf.SERVERSTOP";
    public final static String PARAMETERS_EXTRA = "com.uma.iperf.PARAMETERS";
    public final static String UPDATE_BROADCAST_ACTION = "com.uma.iperf.UPDATEACTION";
    public final static String LOG_BROADCAST_ACTION = "com.uma.iperf.LOGACTION";
    public final static String LOG_IPERF_MODE = "com.uma.iperf.LOGIPERFMODE";
    public final static String LOG_STRING = "com.uma.iperf.LOGSTRING";
    private static String LOG = "iperf." + iPerfService.class.getSimpleName();
    private static String MESSAGE = "iperf.Message";
    private static String APP_FOLDER;
    private static iPerfTask client = null;
    private static iPerfTask server = null;
    private static String lastClientParams = "";
    private static String lastServerParams = "";

    private static int taskId = 0;

    public iPerfService() {
    }

    public static void clientFinished() {
        Log.i(LOG, "Received notification that client has finished.");
        client = null;
    }

    public static void serverFinished() {
        Log.i(LOG, "Received notification that server has finished.");
        server = null;
    }

    public static boolean clientActive() {
        return client != null;
    }

    public static boolean serverActive() {
        return server != null;
    }

    public static String clientParameters() {
        return lastClientParams;
    }

    public static String serverParameters() {
        return lastServerParams;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        APP_FOLDER = this.getFilesDir().getAbsolutePath();
        String action = intent.getAction();
        taskId++;

        if (action == null) {
            Log.w(LOG, "Received intent with no action. Call ignored.");
        } else {
            String extras = intent.getStringExtra(PARAMETERS_EXTRA);
            String[] params = new String[0];
            if (extras != null) {
                Log.i(LOG, "Parameters: " + extras);
                params = extras.split(",");
            }

            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
            int mode = iPerfTask.SERVER;// In case of error, the output is the Application/Server log
            String output = "Error while processing received service request.";

            switch (action) {
                case START_CLIENT_ACTION:
                    mode = iPerfTask.CLIENT;
                    Log.i(LOG, "START_CLIENT_ACTION");
                    if (client != null) {
                        output = "Client iPerf instance already started. Call ignored";
                        Log.i(MESSAGE, "<<<Warning:" + output + ">>>");
                        Log.e(LOG, output);
                    } else {
                        client = new iPerfTask(this, iPerfTask.CLIENT, taskId, APP_FOLDER + "/iperf");
                        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
                        output = "Client started.";
                        lastClientParams = TextUtils.join(" ", params);
                        iPerfActivity.notifyStatusChange(broadcastManager);
                    }
                    break;
                case START_SERVER_ACTION:
                    mode = iPerfTask.SERVER;
                    Log.i(LOG, "START_SERVER_ACTION");
                    if (server != null) {
                        output = "Server iPerf instance already started. Call ignored";
                        Log.i(MESSAGE, "<<<Warning:" + output + ">>>");
                        Log.e(LOG, output);
                    } else {
                        server = new iPerfTask(this, iPerfTask.SERVER, taskId, APP_FOLDER + "/iperf");
                        server.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
                        output = "Server started.";
                        lastServerParams = TextUtils.join(" ", params);
                        iPerfActivity.notifyStatusChange(broadcastManager);
                    }
                    break;
                case STOP_CLIENT_ACTION:
                    mode = iPerfTask.CLIENT;
                    Log.i(LOG, "STOP_CLIENT_ACTION");
                    if (client == null) {
                        output = "No iPerf client is running. Call ignored";
                        Log.i(MESSAGE, "<<<Warning:" + output + ">>>");
                        Log.e(LOG, output);
                    } else {
                        client.cancel(true);
                        client = null;
                        output = "Client stopped";
                        iPerfActivity.notifyStatusChange(broadcastManager);
                    }
                    break;
                case STOP_SERVER_ACTION:
                    mode = iPerfTask.SERVER;
                    Log.i(LOG, "STOP_SERVER_ACTION");
                    if (server == null) {
                        output = "No iPerf server is running. Call ignored";
                        Log.i(MESSAGE, "<<<Warning:" + output + ">>>");
                        Log.e(LOG, output);
                    } else {
                        server.cancel(true);
                        server = null;
                        output = "Server stopped";
                        iPerfActivity.notifyStatusChange(broadcastManager);
                    }
                    break;
            }
            iPerfActivity.broadcast(broadcastManager, mode, output);
        }
        return Service.START_NOT_STICKY;
    }
}
