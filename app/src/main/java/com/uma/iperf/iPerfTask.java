/**
 * Developed by the University of Málaga
 *
 * Alberto Salmerón Moreno
 * Bruno García García
 * Gonzalo Chica Morales
 */

package com.uma.iperf;

import android.content.Context;
import android.os.AsyncTask;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class iPerfTask extends AsyncTask<String, String, Integer> {
    public final static int CLIENT = 0;
    public final static int SERVER = 1;

    private static String LOG = "iperf." + iPerfTask.class.getSimpleName();
    private static String MESSAGE = "iperf.Message";
    private TextView textView;
    private String taskId;
    private String iperf;
    private int taskType;

    private Context context;
    private OutputStreamWriter file;
    private SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_hhmmss");

    public iPerfTask(Context c, int cs, int id, String app) {
        context = c;
        taskType = cs;
        switch (taskType) {
            case CLIENT:
                taskId = "Client<" + id + ">";
                break;
            case SERVER:
                taskId = "Server<" + id + ">";
                break;
        }
        iperf = app;

        fileOpen();
    }

    private void fileOpen() {
        File externalStorage = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        String folderpath = externalStorage.getAbsolutePath() + "/iPerfAgent/";
        String filepath = folderpath + (taskType == CLIENT ? "Client" : "Server") + "_" +
                format.format(Calendar.getInstance().getTime()) + ".txt";

        try {
            File folderHandle = new File(folderpath);
            if (!folderHandle.exists()) {
                Log.i(LOG, "Creating app folder. Success: " + folderHandle.mkdirs());
            }

            File fileHandle = new File(filepath);
            if (!fileHandle.createNewFile()) {
                throw new IOException("File " + filepath + " already exists");
            }

            file = new OutputStreamWriter(new FileOutputStream(fileHandle));
        }
        catch (IOException e) {
            Log.e(LOG, "File open '" + filepath + "' failed: " + e.toString());
        }
    }

    private void fileClose() {
        if (file != null) {
            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                Log.e(LOG, "File close failed: " + e.toString());
            }
        }
    }

    protected Integer doInBackground(String... params) {
        Process process;
        List<String> commandList = new ArrayList<>();
        commandList.add(iperf);
        commandList.addAll(Arrays.asList(params));

        Log.i(LOG, String.format("Starting iperf task <%1s>.\n" +
                " Command:", taskId) + commandList.toString());

        NonBlockingReader reader;
        try {
            process = new ProcessBuilder().command(commandList).redirectErrorStream(true).start();
            reader = new NonBlockingReader(new InputStreamReader(process.getInputStream()));
        } catch (IOException e) {
            String msg = "Error while launching iPerf process: " + e.getMessage();
            Log.i(MESSAGE, "<<<Error:" + msg + ">>>");
            updateLogs(taskId + ": " + msg);
            Log.e(LOG, msg);
            fileClose();
            return -1;
        }

        while (!isCancelled() && !reader.finished()) {
            if (reader.ready()) {
                updateLogs(reader.read());
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Log.d(LOG, "Interrupted while waiting for iPerf process output: " + e.getMessage());
            }
        }

        reader.close();

        // If the process has not finished, accessing the exitValue will
        // throw an exception. In this case we force-close the process.
        try {
            int value = process.exitValue();
            Log.i(LOG, "iPerf return value is: " + value);
        } catch (IllegalThreadStateException e) {
            process.destroy();
            Log.i(LOG, "iPerf process has been terminated.");
        }

        switch (taskType) {
            case CLIENT:
                iPerfActivity.broadcast(LocalBroadcastManager.getInstance(context),
                        CLIENT, "Client process has finished.");
                iPerfService.clientFinished();
                break;
            case SERVER:
                iPerfActivity.broadcast(LocalBroadcastManager.getInstance(context),
                        SERVER, "Server process has finished.");
                iPerfService.serverFinished();
                break;
        }
        iPerfActivity.notifyStatusChange(LocalBroadcastManager.getInstance(context));

        fileClose();
        return 0;
    }

    private void updateLogs(String message) {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);

        iPerfActivity.broadcast(broadcastManager, taskType, message.trim());

        // Required to ensure that each logCat line corresponds to a single line of iPerf output
        String[] lines = message.split("\\r\\n|\\n|\\r");
        for (int l = 0; l < lines.length; l++) {
            if (!lines[l].isEmpty()) {
                String lineStr = "<<< Timestamp: " + System.currentTimeMillis() + " ; Output: "+ lines[l].trim() + " >>>";
                Log.i("iperf." + ((taskType == CLIENT) ? "Client" : "Server"), lineStr);
                try {
                    file.write(lineStr + '\n');
                } catch (IOException e) {
                    Log.e(LOG, "Could not write to file: " + e.toString());
                }

            }
        }
    }
}
